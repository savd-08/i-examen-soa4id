package soa4id.pizzamaker;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.spinner_sabor)
    Spinner mSpinnerSabor;

    @BindView(R.id.button_preparar)
    Button mButtonPreparar;

    @BindView(R.id.text_pizza)
    TextView mTextPizza;

    PizzaFactory pizzaFactory;
    PizzaFactory.TipoPizza tipoPizza;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("Jamón y queso");
        spinnerArray.add("Hawaiana");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(),
                R.layout.support_simple_spinner_dropdown_item, spinnerArray);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        mSpinnerSabor.setAdapter(adapter);
        pizzaFactory= new AppPizzaFactory();
    }

    @OnClick(R.id.button_preparar)
    void onClick(View v) {
        Pizza pizza = pizzaFactory.getPizza(tipoPizza);
        mTextPizza.setText(pizza.preparar());
    }

    @OnItemSelected(R.id.spinner_sabor)
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        switch(pos){
            case  0:
                tipoPizza = PizzaFactory.TipoPizza.JAMON_QUESO;
                break;
            case  1:
                tipoPizza = PizzaFactory.TipoPizza.HAWAIANA;
                break;
            default:
                tipoPizza = PizzaFactory.TipoPizza.JAMON_QUESO;
                break;
        }
    }
}


