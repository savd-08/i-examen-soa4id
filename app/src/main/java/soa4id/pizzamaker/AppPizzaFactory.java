package soa4id.pizzamaker;

/**
 * Created by sergio on 11/10/17.
 */

public class AppPizzaFactory implements PizzaFactory {
    @Override
    public Pizza getPizza(TipoPizza tipoPizza){
        Pizza pizza;

        switch(tipoPizza){
            case JAMON_QUESO:
                pizza = new PizzaJamonQueso(new PizzaJamonQueso.Builder("Clasica", "Italiana")
                .agregarIngrediente("Jamón").agregarIngrediente("Queso"));
                break;
            case HAWAIANA:
                pizza = new PizzaHawaiana(new PizzaHawaiana.Builder("Integral", "Italiana")
                        .agregarIngrediente("Jamón").agregarIngrediente("Queso")
                        .agregarIngrediente("Piña").agregarIngrediente("Coco"));
                break;
            default:
                pizza = new PizzaJamonQueso(new PizzaJamonQueso.Builder("Clasica", "Italiana")
                        .agregarIngrediente("Jamón").agregarIngrediente("Queso"));
                break;
        }

        return pizza;
    }
}


