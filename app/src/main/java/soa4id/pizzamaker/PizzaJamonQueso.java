package soa4id.pizzamaker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sergio on 11/10/17.
 */

public class PizzaJamonQueso implements Pizza{

    private final String nombre = "Jamón y queso";
    private String pasta;
    private String salsa;
    private List<String> ingredientes;

    @Override
    public String preparar(){
        return "Nombre: " + this.nombre + "\nPasta: " + this.pasta +
                "\nSalsa: " + this.salsa + "\nIngredientes:\n\t" + obtenerIngredientes();
    }

    @Override
    public String obtenerIngredientes() {
        StringBuilder sb = new StringBuilder();
        for (String s : this.ingredientes)
        {
            sb.append(s);
            sb.append("\n\t");
        }
        return sb.toString();
    }

    public PizzaJamonQueso(Builder builder){
        this.pasta = builder.pasta;
        this.salsa = builder.salsa;
        this.ingredientes = builder.ingredientes;
    }

    public static class Builder {
        private final String pasta;
        private final String salsa;
        private List<String> ingredientes;

        public Builder(String pasta, String salsa){
            this.pasta = pasta;
            this.salsa = salsa;
            this.ingredientes = new ArrayList<>();
        }

        public Builder agregarIngrediente(String ingrediente){
            this.ingredientes.add(ingrediente);
            return this;
        }
    }
}


