package soa4id.pizzamaker;

import java.util.List;

/**
 * Created by sergio on 11/10/17.
 */

public interface Pizza {

    String preparar();

    String obtenerIngredientes();

}
