package soa4id.pizzamaker;

/**
 * Created by sergio on 11/10/17.
 */

public interface PizzaFactory {
    enum TipoPizza{
        JAMON_QUESO, HAWAIANA
    }

    Pizza getPizza(TipoPizza tipoPizza);
}
